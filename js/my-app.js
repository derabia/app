// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon:true
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: true,
    // Enable Dom Cache so we can use all inline pages
    domCache: true
});

$$('.cat-popup').on('click', function () {
  myApp.popup('.popup-cat');
});

$$('.sales-popup').on('click', function () {
  myApp.popup('.sales-popup');
});

$(".filter").hide();
$('#filter').on('click', function () {
    $(".filter").slideToggle(100);
});

$$('#tab1').on('show', function () {
    $$(".filter").hide();
});

$$('#tab2').on('show', function () {
    $$(".filter").hide();
});

$$('.open-about').on('click', function () {
  myApp.popup('.popup-about');
});

$$('.demo-progressbar-inline .button').on('click', function () {
    var progress = $$(this).attr('data-progress');
    var progressbar = $$('.demo-progressbar-inline .progressbar');
    myApp.setProgressbar(progressbar, progress);
});

var mySearchbar = app.f7.searchbar('.searchbar', {
        customSearch: true
    });
    $$('.searchbar').on("search", function(e) {
        console.log("search " + e.target.value);
        mySearchbar.disable();
    });
